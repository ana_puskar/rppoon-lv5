using System;

namespace RPPOON_LV5_AP
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lightTheme = new LightTheme();
            MyTheme myTheme = new MyTheme();
            ReminderNote note = new ReminderNote("Breathe.", lightTheme);
            note.Show();
            note = new ReminderNote("I wanna see the sea.", myTheme);
            note.Show();
        }
    }
}