using System;

namespace RPPOON_LV5_AP
{
    class Program
    {
        static void Main(string[] args)
        {
            Product pen = new Product("blue pen", 0.2, 2.99);
            Product mug = new Product("favourite tea mug", 0.3, 5.89);
            Product pencil = new Product("2B pencil", 0.15, 1.73);
            
            Box listOfProducts = new Box("products");

            listOfProducts.Add(pen);
            listOfProducts.Add(mug);
            listOfProducts.Add(pencil);

            Console.WriteLine("Product description: " + listOfProducts.Description());
            Console.WriteLine("Product price: " + listOfProducts.Price);
            Console.WriteLine("Product weight: " + listOfProducts.Weight);
           
        }
    }
}