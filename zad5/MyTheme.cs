using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV5_AP
{
    class MyTheme : ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.Cyan;
        }
        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
        }
        public string GetHeader(int width)
        {
            return new string('~', width);
        }
        public string GetFooter(int width)
        {
            return new string('~', width);
        }
    }
}
