using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV5_AP
{
    class ShippingService
    {
        private double pricePerKg;
        public ShippingService(double pricePerKg)
        {
            this.pricePerKg = pricePerKg;
        }
        public double GetPrice(IShipable box)
        {
            return box.Weight * this.pricePerKg;
        }
    }
}